import time

import networkx as nx
from networkx.algorithms import approximation
import numpy as np
import os

def Read_Graph_from_textFile(inputPath):
    graphs = []
    G = nx.Graph()
    with open(inputPath, 'r') as file:
        line = file.readline()
        while line is not None and line != "":
            if (line[0] != '-'):
                nodes = line.split("\t")
                G.add_edge(int(nodes[0]), int(nodes[1]))
            else:
                G.remove_edges_from(G.selfloop_edges())
                graphs.append(G)
                G = nx.Graph()
            line = file.readline()

    if(len(graphs) == 100):
        print("Num Check: OK")
        return graphs
    else:
        print("Num Check: Error")
        exit(-1)

def Output_Graph_textFile(G, outputPath, node_num, edge_num):
    with open(outputPath, 'wb') as file:
        log = "p tw " + str(node_num) + " " + str(edge_num) + "\n"
        file.write(log.encode())
        relabeled_G = nx.convert_node_labels_to_integers(G, first_label=1)
        nx.write_edgelist(relabeled_G, file, data=False, delimiter=' ')

def main():
    hop = 3
    # datasets = ["undirected_WikiTalk", "undirected_Email-EuAll", "undirected_roadNet-PA", "undirected_roadNet-TX", "undirected_roadNet-CA"]
    datasets = ["undirected_Email-EuAll", "undirected_roadNet-PA", "undirected_roadNet-TX", "undirected_roadNet-CA"]

    graphs = []
    for filename in datasets:
        datapath = "../hop_data/" + filename
        for i in range(hop):
            path = datapath + "_" + str(i + 1) + "Hop.txt"
            graphs = Read_Graph_from_textFile(path)
            for graph_idx, eval_graph in enumerate(graphs):
                OutputFileDir = "../converted_data_without_selfLoop/"
                if not os.path.isdir(OutputFileDir):
                    os.mkdir(OutputFileDir)
                OutputFileDir += filename + "/"
                if not os.path.isdir(OutputFileDir):
                    os.mkdir(OutputFileDir)
                OutputFileDir += str(i + 1) + "Hop" 
                if not os.path.isdir(OutputFileDir):
                    os.mkdir(OutputFileDir)
                outputPath = OutputFileDir + "/" + str(graph_idx) + ".gr"
                node_num, edge_num = eval_graph.number_of_nodes(), eval_graph.number_of_edges()
                Output_Graph_textFile(eval_graph, outputPath, node_num, edge_num)
                print("Process: " + str(path) + ", " + str(graph_idx) + "/100")
            print(path)

if __name__ == '__main__':
    main()
