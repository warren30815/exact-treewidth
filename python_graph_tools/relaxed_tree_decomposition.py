##  implementation of relaxed tree decomposition in https://openproceedings.org/2012/conf/edbt/AkibaSK12.pdf
##  Created by 許竣翔 on 2019/12/06.
##  Copyright © 2019年 許竣翔. All rights reserved.
##  Email: qaz7821819@gmail.com

import networkx as nx
import os
import unittest
from joblib import Parallel, delayed
import multiprocessing

class DecomposeGraph():
	def __init__(self, G, w):
		self.G = nx.Graph(G)
		self.tree_G = nx.Graph()
		self.tree_G_bag_labels_mapping = {}
		self.w = w
		self.bag_list = []
		self.root_bag_index = 0 

	def pickup_qualified_node(self, current_bound):
		# region end of all iterations signal
		if self.G.number_of_nodes() - 1 <= current_bound:
			return -1
		# endregion
		G_degree = self.G.degree()
		sorted_G_degree = sorted(G_degree, key=lambda x: x[1])

		for node, val in sorted_G_degree:
			if val <= current_bound:
				# print("node: " + str(node) + " is selected")
				return node
			else:
				pass

		# end of this round iteration
		return None

	def generate_bag(self, node):
		neighbors = [n for n in self.G[node]]
		neighbors.append(node)
		bag = frozenset(neighbors)
		return bag, node

	def remove_processed_node(self, node):
		self.G.remove_node(node)

	def construct_tree(self, current_root_bag, child_bag, child_node, current_child_bag_index):
		root_bag_index = self.root_bag_index
		tree_G = self.tree_G
		tree_G_bag_labels_mapping = self.tree_G_bag_labels_mapping

		root_neighbors = []
		if len(tree_G.nodes) != 0:
			root_neighbors = [n for n in tree_G[root_bag_index]]

		tree_G.add_edge(root_bag_index, current_child_bag_index)

		for nbr in root_neighbors:
			if child_node in tree_G_bag_labels_mapping[nbr]:
				tree_G.remove_edge(root_bag_index, nbr)
				tree_G.add_edge(current_child_bag_index, nbr)

		# update labels mapping
		tree_G_bag_labels_mapping[root_bag_index] = current_root_bag
		tree_G_bag_labels_mapping[current_child_bag_index] = child_bag
			# if (all(x in root_bag for x in nbrs)):

	def iterate_ReduceVertex(self):
		current_child_bag_index = 1
		for current_bound in range(self.w + 1):
			# print("Process: " + str(current_bound) + "/" + str(self.w))
			while 1:
				selected_node = self.pickup_qualified_node(current_bound)
				if selected_node != None:
					if selected_node != -1:
						bag, child_node = self.generate_bag(selected_node)
						# print("bag info: " + str(bag.edges))
						self.bag_list.append(bag)
						self.remove_processed_node(selected_node)
						current_root_bag = frozenset(self.G.nodes)
						self.construct_tree(current_root_bag, bag, child_node, current_child_bag_index)
						current_child_bag_index += 1
					else:
						# end of all iterations
						return 
				# end of this round iteration
				else:
					break

	def construct_root_bag(self):
		root_G = self.G
		root_bag_index = self.root_bag_index
		root_bag = frozenset(root_G.nodes)
		self.bag_list.append(root_bag)

		if len(self.tree_G.nodes) == 0:
			self.tree_G.add_node(root_bag_index)
			self.tree_G_bag_labels_mapping[root_bag_index] = root_bag

	def get_result(self):
		# print("iterate_ReduceVertex...")
		self.iterate_ReduceVertex()
		# print("construct_root_bag...")
		self.construct_root_bag()

		# print(self.tree_G_bag_labels_mapping)
		partial_tw = 0
		for bag in self.bag_list:
			tw = len(bag) - 1
			if tw <= self.w:
				if tw > partial_tw:
					partial_tw = tw

			if partial_tw == self.w:
				break

		return partial_tw, self.tree_G, self.tree_G_bag_labels_mapping
 

class DecomposeGraphTestCase(unittest.TestCase):
    def setUp(self):
    	G = nx.Graph()
    	G.add_edge(3,4)
    	G.add_edge(0,4)
    	G.add_edge(0,2)
    	G.add_edge(2,4)
    	G.add_edge(1,2)
    	G.add_edge(1,5)
    	G.add_edge(1,6)
    	G.add_edge(5,6)
    	G.add_edge(2,6)
    	G.add_edge(4,6)
    	self.deGraph = DecomposeGraph(G=G, w=2)

    def test_plus(self):
        expected = 2
        is_tree = False
        result, tree_G, tree_G_bag_labels_mapping = self.deGraph.get_result()
        print(tree_G_bag_labels_mapping)
        print(tree_G.edges)
        is_tree = nx.is_tree(tree_G)
        self.assertEqual(expected, result)
        self.assertEqual(is_tree, True)

# unittest.main()
#exit()

def Read_Graph(inputDir):
	graphs = []
	G = nx.Graph()
	if os.path.isdir(inputDir):
		list_dirs = os.walk(inputDir) 
		for _, _, files in list_dirs:    
		    for f in files: 
		        inputPath = os.path.join(inputDir, f)
		        with open(inputPath, 'r') as file:
			        line = file.readline()
			        while line is not None and line != "":
			            if (line[0] != "p"):
			                nodes = line.split(" ")
			                G.add_edge(int(nodes[0]), int(nodes[1]))
			            else:
			                pass
			            line = file.readline()

			        graphs.append(G)
			        G = nx.Graph()
	else:
		print("input isn't a dir path!!")
		exit(-1)

	return graphs

def without_hop_Read_Graph(inputPath):
    graphs = []
    G = nx.Graph() 

    with open(inputPath, 'r') as file:
        line = file.readline()
        while line is not None and line != "":
            nodes = line.split("\t")
            G.add_edge(int(nodes[0]), int(nodes[1]))
            line = file.readline()

        graphs.append(G)
        G = nx.Graph()

    print("Preprocess done")
    return graphs

def Output_txt_processing_dataset_name(dataset_name, outputPath):
	with open(outputPath, 'ab') as file:
		description_log =  "c " + dataset_name + "\n" 
		file.write(description_log.encode())

def Output_Graph_txtFile(w, outputPath, partial_tw, tree_G, tree_G_bag_labels_mapping, node_num, edge_num):
	with open(outputPath, 'ab') as file:
		description_log = "d " + w + " " + partial_tw + " " + str(len(tree_G_bag_labels_mapping)) + " " + node_num + " " + edge_num + "\n"
		file.write(description_log.encode())
		bag_str = ""
		for bag_index in tree_G_bag_labels_mapping:
			bag = tree_G_bag_labels_mapping[bag_index]
			bag_str += "b " + str(bag) + "\n"
		if len(tree_G.edges) != 0:
			for edge in tree_G.edges:
				bag_str += "e " + str(edge[0]) + " " + str(edge[1]) + "\n"  
		else:
			bag_str += "e -1" + "\n"
		separation_log = "-----" + "\n"
		bag_str += separation_log
		file.write(bag_str.encode())

def Output_csvFile(outputPath, dataset_name, hop_num, w, avg_partial_tw, avg_root_bag_size, avg_root_bag_nbrs, avg_original_node_num, avg_original_edge_num):
	with open(outputPath, 'ab') as file:
		description_log = dataset_name + "," + hop_num + "," + w + "," + avg_partial_tw + "," + avg_root_bag_size + "," + avg_root_bag_nbrs + "," + avg_original_node_num + "," + avg_original_edge_num + "\n"
		file.write(description_log.encode())

def get_output_file(path, dataset_name, hop_num, lower_w, upper_w, txt_outputPath, csv_outputPath):
    sample_num = 100
    graphs = Read_Graph(path)
    Output_txt_processing_dataset_name(dataset_name, txt_outputPath)
    for w in range(lower_w, upper_w + 1):
        total_partial_tw = 0
        total_root_bag_size = 0
        total_root_bag_nbrs = 0
        total_original_node_num = 0
        total_original_edge_num = 0
        for graph_idx, eval_graph in enumerate(graphs):
            deGraph = DecomposeGraph(G=eval_graph, w=w)
            partial_tw, tree_G, tree_G_bag_labels_mapping = deGraph.get_result()
            root_bag_index = deGraph.root_bag_index
            root_bag_size = len(tree_G_bag_labels_mapping[root_bag_index])
            root_bag_nbrs = len([n for n in tree_G[root_bag_index]])
            node_num, edge_num = eval_graph.number_of_nodes(), eval_graph.number_of_edges()
            Output_Graph_txtFile(str(w), txt_outputPath, str(partial_tw), tree_G, tree_G_bag_labels_mapping, str(node_num), str(edge_num))
            total_partial_tw += partial_tw
            total_root_bag_size += root_bag_size
            total_root_bag_nbrs += root_bag_nbrs
            total_original_node_num += node_num
            total_original_edge_num += edge_num
            print("Process: " + str(dataset_name) + ", w: " + str(w) + " -> " + str(graph_idx + 1) + "/100")
        avg_partial_tw = total_partial_tw / sample_num
        avg_root_bag_size = total_root_bag_size / sample_num
        avg_root_bag_nbrs = total_root_bag_nbrs / sample_num
        avg_original_node_num = total_original_node_num / sample_num
        avg_original_edge_num = total_original_edge_num / sample_num
        Output_csvFile(csv_outputPath, dataset_name, str(hop_num), str(w), str(avg_partial_tw), str(avg_root_bag_size), str(avg_root_bag_nbrs), str(avg_original_node_num), str(avg_original_edge_num))    

def get_output_file_without_hop(path, dataset_name, w, txt_outputPath, csv_outputPath):
    graphs = without_hop_Read_Graph(path)
    eval_graph = graphs[0]
    Output_txt_processing_dataset_name(dataset_name, txt_outputPath)
    deGraph = DecomposeGraph(G=eval_graph, w=w)
    partial_tw, tree_G, tree_G_bag_labels_mapping = deGraph.get_result()
    root_bag_index = deGraph.root_bag_index
    root_bag_size = len(tree_G_bag_labels_mapping[root_bag_index])
    root_bag_nbrs = len([n for n in tree_G[root_bag_index]])
    node_num, edge_num = eval_graph.number_of_nodes(), eval_graph.number_of_edges()
    Output_Graph_txtFile(str(w), txt_outputPath, str(partial_tw), tree_G, tree_G_bag_labels_mapping, str(node_num), str(edge_num))
    print("Process: " + str(dataset_name) + ", w: " + str(w))
    # Output_csvFile(csv_outputPath, dataset_name, "all", str(w), str(partial_tw), str(root_bag_size), str(root_bag_nbrs), str(node_num), str(edge_num))    

def main():
    # hop = ["1", "2", "3"]
    lower_w = 5
    upper_w = 5

    # datasets = ["version2_Enron", "version2_facebook_combined", "version2_undirected_soc-Epinions1", "version2_undirected_soc-LiveJournal1", "version2_undirected_soc-pokec-relationships", "version2_undirected_twitter_combined"]
    # datasets = ["undirected_WikiTalk", "undirected_Email-EuAll", "undirected_roadNet-PA", "undirected_roadNet-TX", "undirected_roadNet-CA", "p2p-Gnutella31", "p2p-Gnutella30", "p2p-Gnutella25", "p2p-Gnutella24", "undirected_CA-HepTh"]
    datasets = ["email-Enron", "com-youtube.ungraph", "com-orkut.ungraph", "com-lj.ungraph", "wiki-topcats"]

    txt_outputPath = "../result/parallel_without_hop_whole_partial_tw.txt"  # todo
    # csv_outputPath = "../result/parallel_without_hop_average_partial_tw.csv"

    for filename in datasets:
        # datapath = "../converted_data_without_selfLoop/" + filename + "/"
        # for hop_num in hop:
        #     hop_dir = hop_num + "Hop"
        #     dataset_name = filename + "_" + hop_dir
        #     path = datapath + hop_dir
        #     get_output_file(path, dataset_name, hop_num, lower_w, upper_w, txt_outputPath, csv_outputPath)
        datapath = "../../treewidth_data/" + filename + ".txt"  # todo
        num_cores = multiprocessing.cpu_count()
        squares = Parallel(n_jobs=num_cores, verbose=50)(delayed(
            get_output_file_without_hop)(datapath, filename, w, txt_outputPath, "") for w in range(lower_w, upper_w+1))

if __name__ == '__main__':
    main()

# unittest.main()