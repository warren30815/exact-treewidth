/*
 * Copyright (c) 2017, Hisao Tamaki
 */

package tw.exact;

import java.io.File;



import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.io.FileWriter;
import java.io.IOException;

public class MainDecomposer {
  private static boolean VERBOSE = false;
//  private static boolean VERBOSE = true;
  private static boolean DEBUG = false;
//private static boolean debug = true;

  private static long time0;

  public static TreeDecomposition decompose(Graph g) {
    log("decompose n = " + g.n);
    if (g.n == 0) {
      TreeDecomposition td = new TreeDecomposition(0, -1, g);
      return td;
    }

    ArrayList<XBitSet> components = g.getComponents(new XBitSet());
    
    int nc = components.size();
    if (nc == 1) {
      return decomposeConnected(g);      
    }
    
    int invs[][] = new int[nc][];
    Graph graphs[] = new Graph[nc];
    
    for (int i = 0; i < nc; i++) {
      XBitSet compo = components.get(i);
      int nv = compo.cardinality();
      graphs[i] = new Graph(nv);
      invs[i] = new int[nv];
      int conv[] = new int[g.n];
      int k = 0;
      for (int v = 0; v < g.n; v++) {
        if (compo.get(v)) {
          conv[v] = k;
          invs[i][k] = v;
          k++;
        }
        else {
          conv[v] = -1;
        }
      }
      graphs[i].inheritEdges(g, conv, invs[i]);
    }

    TreeDecomposition td = new TreeDecomposition(0, 0, g);
    
    for (int i = 0; i < nc; i++) {
      TreeDecomposition td1 = decomposeConnected(graphs[i]);
      if (td1 == null) {
        return null;
      }
      td.combineWith(td1, invs[i], null);
    }
    return td;
  }
  
  public static TreeDecomposition decomposeConnected(Graph g) {
    log("decomposeConnected: n = " + g.n);

    if (g.n <= 2) {
      TreeDecomposition td = new TreeDecomposition(0, g.n - 1, g);
      td.addBag(g.all.toArray());
      return td;
    }
    
    Bag best = null;
    
    GreedyDecomposer.Mode[] modes = 
        new GreedyDecomposer.Mode[]{
            GreedyDecomposer.Mode.fill,
            GreedyDecomposer.Mode.defect,
            GreedyDecomposer.Mode.degree
            };
    
    for (GreedyDecomposer.Mode mode: modes) { 
      Bag whole = new Bag(g); 

      GreedyDecomposer mfd = new GreedyDecomposer(whole, mode);
//      GreedyDecomposer mfd = new GreedyDecomposer(whole);

      mfd.decompose();

      log("greedy decomposition (" + mode + ") obtained with " +
            whole.nestedBags.size() + " bags and width " + 
            whole.width);

      whole.detectSafeSeparators();

      log(whole.countSafeSeparators() + " safe separators found ");

      whole.validate();
      
      whole.pack();
      
      whole.validate();

      log("the decomposition packed into " +
            whole.nestedBags.size() + " bags, separatorWidth = " + 
            whole.separatorWidth + ", max bag size = " + 
            whole.maxNestedBagSize());

      if (best == null ||
          whole.maxNestedBagSize() < best.maxNestedBagSize()) {
        best = whole;
      }
    }
//    best = whole;
   
    //    whole.dump();

    int lowestPossible = best.separatorWidth;
    
    for (Bag bag: best.nestedBags) {
      if (bag.getWidth() > lowestPossible) {
        bag.makeRefinable();
        IODecomposer mtd = new IODecomposer(bag, g.minDegree(), g.n - 1);
        mtd.decompose();
        int w = bag.getWidth();
        if (w > lowestPossible) {
          lowestPossible = w;
        }
      }
    }
    
    log("flattening");
    
    best.flatten();

    log("the decomposition flattened into " +
          best.nestedBags.size() + " bags");
    
//    whole.dump();
    
    return best.toTreeDecomposition();
  }
  
  static void log(String message) {
    if (VERBOSE) {
      System.out.println(message);
    }
  }

  public static void writeTotxt(String dir, String hop, String tw, String node_num, String edge_num, String density){
    try(FileWriter fw = new FileWriter("../result/full_exact_tw.csv", true);
    BufferedWriter bw = new BufferedWriter(fw);
    PrintWriter out = new PrintWriter(bw))
    {
        fw.write(dir);
        fw.write(",");
        fw.write(hop);
        fw.write(",");
        fw.write(tw);
        fw.write(",");
        fw.write(node_num);
        fw.write(",");
        fw.write(edge_num);
        fw.write(",");
        fw.write(density);
        fw.write("\n");
    } 
    catch (IOException e) 
    {
      System.out.print("Error!!!");
      System.exit(1);
        //exception handling left as an exercise for the reader
    }
  }

  public static void main(String[] args) throws IOException {
    // int total_tw = 0;
    int current_num = 0;
    int total_num = 100;
    File[] dataDir = new File("/home/vaplab/Desktop/exact_treewidth/converted_data_without_selfLoop").listFiles();
    String[] hops = new String[]{"1", "2"};
    // String[] hops = new String[]{"3"};
    for (File dir : dataDir) {
      if (dir.isDirectory()) {
          for (String hop_num : hops) {
            String hop = hop_num + "Hop";
            String hop_dir = dir + "/" + hop;
            File[] each_data = new File(hop_dir).listFiles();
            // long start_time = System.currentTimeMillis();
            for (File graph_data_path : each_data) {
              if (!graph_data_path.isDirectory()) {
                System.out.print(graph_data_path);
                Graph g = Graph.readGraph(graph_data_path);
                TreeDecomposition td = decompose(g);
                // total_tw += td.getExactWidth();
                int tw = td.getExactWidth();
                int num_node = g.n;
                int num_edge = g.numberOfEdges();
                float density = (float)(2 * num_edge) / (num_node * (num_node - 1));
                writeTotxt(dir.getName(), hop_num, String.valueOf(tw), String.valueOf(num_node), String.valueOf(num_edge), String.valueOf(density));
                current_num += 1;
                System.out.println(dir + ", Process: "+ String.valueOf(current_num));
              }
            }
            current_num = 0;
            // long elapsed_sec = (System.currentTimeMillis() - start_time) / 1000;
            // if (current_num == total_num){
            //   writeTotxt(hop_dir, String.valueOf(total_tw / total_num), String.valueOf(elapsed_sec));
            //   total_tw = 0;
            //   current_num = 0;
            // }
            // else{
            //   System.out.print("Num Error!!");
            //   System.exit(1);
            // }
          
          }
      }
    }
  }
}
